import {AfterViewInit, Component, OnInit} from '@angular/core';
import {HttpService} from "./http.service";
import {environment} from "../environments/environment";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements  OnInit{
  response: any = {};
  bannerStyle = `background-color: ${environment.bannerColor}`

  constructor(private httpService: HttpService) {
    console.log(environment)
  }


  ngOnInit(): void {
    this.httpService.getData().subscribe(value => {
      this.response = value
    })
  }


}
