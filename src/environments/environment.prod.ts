export const environment = {
  production: true,
  bannerColor: (window as any)["env"]["BANNER_COLOR"],
  apiUrl: (window as any)["env"]["API_URL"] || 'http://localhost:8080/hello'
};
