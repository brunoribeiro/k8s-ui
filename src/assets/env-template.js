const global = window;
(function (window) {
    window.env = window.env || {};
    window['env']['BANNER_COLOR'] = '${BANNER_COLOR}';
    window['env']['API_URL'] = '${API_URL}';
})(this);
