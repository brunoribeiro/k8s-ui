FROM nginx:alpine
COPY dist/ui /usr/share/nginx/html/
CMD ["/bin/sh",  "-c",  "envsubst < /usr/share/nginx/html/assets/env-template.js > /usr/share/nginx/html/assets/env.js && exec nginx -g 'daemon off;'"]
